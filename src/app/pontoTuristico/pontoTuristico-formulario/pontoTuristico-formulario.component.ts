import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { PontoTuristico } from '../pontoTuristico';
import { PontoTuristicoService } from '../pontoTuristico.service';
import { CategoriaTuristica } from '../../categoriaTuristica/categoriaTuristica';
import { CategoriaTuristicaService } from '../../categoriaTuristica/categoriaTuristica.service';

@Component({
  selector: 'pontoTuristico-lista',
  templateUrl: './pontoTuristico-formulario.component.html',
  styleUrls: ['./pontoTuristico-formulario.component.css']
})
export class PontoTuristicoFormularioComponent implements OnInit {

  pontoTuristico: PontoTuristico;
  pontoTuristicoForm: FormGroup;
  titulo: string;
  categoriaTuristicas: CategoriaTuristica[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private pontoTuristicoService: PontoTuristicoService,
    private categoriaTuristicaService: CategoriaTuristicaService
  ) { }

  ngOnInit() {

    this.pontoTuristico = new PontoTuristico();

    /* Obter o `ID` passado por parâmetro na URL */
    this.pontoTuristico.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.pontoTuristico.id == null)
    ? 'Novo Ponto Turistico'
    : 'Alterar Ponto Turistico';

    /* Reactive Forms */
    this.pontoTuristicoForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      latitude: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      longitude: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      descricao: this.builder.control('', [Validators.required, Validators.maxLength(250)]),
      categoriaTuristica: this.builder.control('', [Validators.required])
    }, {});

    // busca as categorias
    this.categoriaTuristicaService.buscarTodos().subscribe( resposta => {
      this.categoriaTuristicas = resposta;
    })

    // Se existir `ID` realiza busca para trazer os dados
    if (this.pontoTuristico.id != null) {
      this.pontoTuristicoService.buscarPeloId(this.pontoTuristico.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.pontoTuristicoForm.patchValue(retorno);

        });
    }

  }

  salvar(pontoTuristico: PontoTuristico) {
    if (this.pontoTuristicoForm.invalid) {
      console.log("Erro no formulário");
    } 
    else {
      this.pontoTuristicoService.salvar(pontoTuristico)
      .subscribe(response => {
        console.log("Ponto Turistico salva com sucesso");

        // retorna para a lista
        this.router.navigate(['/pontoTuristico']);
      },
      (error) => {
        console.log(error);
        console.log("Erro no back-end");
      });
    }
  }

  compararFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
