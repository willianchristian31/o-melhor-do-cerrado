import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { PontoTuristicoListaComponent } from './pontoTuristico-lista/pontoTuristico-lista.component';
import { PontoTuristicoFormularioComponent } from './pontoTuristico-formulario/pontoTuristico-formulario.component';

const pontoTuristicoRoutes: Routes = [
    { path: '', component: PontoTuristicoListaComponent},
    { path: 'visualizar/:id', component: PontoTuristicoFormularioComponent},
    { path: 'novo', component: PontoTuristicoFormularioComponent},
    { path: 'alterar/:id', component: PontoTuristicoFormularioComponent},
];


@NgModule({
    imports: [RouterModule.forChild(pontoTuristicoRoutes)],
    exports: [RouterModule]
  })

  export class PontoTuristicoRouting {}