import {CategoriaTuristica} from '../categoriaTuristica/categoriaTuristica';

export class PontoTuristico {
  id: number;
  nome: string;
  descricao: string;
  latitude: string;
  longitude: string;
  categoriaTuristica: CategoriaTuristica;
}
