import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { PontoTuristicoRouting } from './pontoTuristico.routing';
import { PontoTuristicoService } from './pontoTuristico.service';
import { PontoTuristicoListaComponent } from './pontoTuristico-lista/pontoTuristico-lista.component';
import { PontoTuristicoFormularioComponent } from './pontoTuristico-formulario/pontoTuristico-formulario.component';
import { CategoriaTuristicaService } from '../categoriaTuristica/categoriaTuristica.service';

@NgModule({
    declarations: [
        PontoTuristicoListaComponent,
        PontoTuristicoFormularioComponent
    ],
    imports: [
        // Angular
        HttpModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        PontoTuristicoRouting
    ],
    providers: [
        // Serviços
        CategoriaTuristicaService,
        PontoTuristicoService
    ]
})

export class PontoTuristicoModule { }
