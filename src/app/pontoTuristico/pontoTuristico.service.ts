import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { PontoTuristico } from './pontoTuristico';

@Injectable()
export class PontoTuristicoService {

    private URL:string = "http://localhost:8888";

    constructor(private http: HttpClient) { }

    buscarTodos(): Observable<PontoTuristico[]> {
        return this.http
            .get<PontoTuristico[]>(`${this.URL}/pontoTuristico`);
    }

    buscarPeloId(id: number): Observable<PontoTuristico> {
        return this.http
            .get<PontoTuristico>(`${this.URL}/pontoTuristico/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(pontoTuristico: PontoTuristico): Observable<PontoTuristico> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (pontoTuristico.id) {
            return this.http
                .put<PontoTuristico>(
                    `${this.URL}/pontoTuristico`,
                    JSON.stringify(pontoTuristico),
                    httpOptions
                );
        } else {
            return this.http
                .post<PontoTuristico>(`${this.URL}/pontoTuristico`, JSON.stringify(pontoTuristico), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${this.URL}/pontoTuristico/${id}`);
    }

}
