import { Component, OnInit } from '@angular/core';

import { PontoTuristico } from '../pontoTuristico';
import { PontoTuristicoService } from '../../pontoTuristico/pontoTuristico.service';
import { Router } from '@angular/router';

@Component({
  selector: 'pontoTuristico-lista',
  templateUrl: './pontoTuristico-lista.component.html',
  styleUrls: ['./pontoTuristico-lista.component.css']
})
export class PontoTuristicoListaComponent implements OnInit {

    pontoTuristicos: PontoTuristico[];

    constructor(
      private pontoTuristicoService: PontoTuristicoService,
      private router: Router
    ){}

    ngOnInit() {
        
      this.pontoTuristicoService.buscarTodos()
      .subscribe(resposta => {
        this.pontoTuristicos = resposta
      });

    }

    excluir(pontoTuristicoId: number) {
      this.pontoTuristicoService.excluir(pontoTuristicoId)
      .subscribe(resposta => {
        console.log("Ponto Turistico excluída com sucesso");
        // retorna para a lista
        this.router.navigate(['/pontoTuristico']);
      } );
    }

}
