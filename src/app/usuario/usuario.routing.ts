import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { UsuarioListaComponent } from './usuario-lista/usuario-lista.component';
import { UsuarioFormularioComponent } from './usuario-formulario/usuario-formulario.component';

const usuarioRoutes: Routes = [
    { path: '', component: UsuarioListaComponent},
    { path: 'visualizar/:id', component: UsuarioFormularioComponent},
    { path: 'novo', component: UsuarioFormularioComponent},
    { path: 'alterar/:id', component: UsuarioFormularioComponent},
];


@NgModule({
    imports: [RouterModule.forChild(usuarioRoutes)],
    exports: [RouterModule]
  })

  export class UsuarioRouting {}
