import {Pessoa} from '../pessoa/pessoa';

export class Usuario {
  id: number;
  nomeUsuario: string;
  permissao: string;
  senha: string;
  pessoa: Pessoa;
}
