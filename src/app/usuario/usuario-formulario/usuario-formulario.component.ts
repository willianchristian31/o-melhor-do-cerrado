import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';
import { Pessoa } from '../../pessoa/pessoa';
import { PessoaService } from '../../pessoa/pessoa.service';

@Component({
  selector: 'usuario-lista',
  templateUrl: './usuario-formulario.component.html',
  styleUrls: ['./usuario-formulario.component.css']
})
export class UsuarioFormularioComponent implements OnInit {

  usuario: Usuario;
  usuarioForm: FormGroup;
  titulo: string;
  pessoas: Pessoa[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private usuarioService: UsuarioService,
    private pessoaService: PessoaService
  ) { }

  ngOnInit() {

    this.usuario = new Usuario();

    /* Obter o `ID` passado por parâmetro na URL */
    this.usuario.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.usuario.id == null)
    ? 'Novo Ponto Turistico'
    : 'Alterar Ponto Turistico';

    /* Reactive Forms */
    this.usuarioForm = this.builder.group({
      id: [],
      nomeUsuario: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      permissao: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      senha: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      pessoa: this.builder.control('', [Validators.required])
    }, {});

    // busca as categorias
    this.pessoaService.buscarTodos().subscribe( resposta => {
      this.pessoas = resposta;
    })

    // Se existir `ID` realiza busca para trazer os dados
    if (this.usuario.id != null) {
      this.usuarioService.buscarPeloId(this.usuario.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.usuarioForm.patchValue(retorno);

        });
    }

  }

  salvar(usuario: Usuario) {
    if (this.usuarioForm.invalid) {
      console.log("Erro no formulário");
    } 
    else {
      this.usuarioService.salvar(usuario)
      .subscribe(response => {
        console.log("Usuário salva com sucesso");

        // retorna para a lista
        this.router.navigate(['/usuario']);
      },
      (error) => {
        console.log(error);
        console.log("Erro no back-end");
      });
    }
  }

  compararFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
