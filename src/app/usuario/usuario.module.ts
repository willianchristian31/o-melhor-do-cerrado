import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { UsuarioRouting } from './usuario.routing';
import { UsuarioService } from './usuario.service';
import { UsuarioListaComponent } from './usuario-lista/usuario-lista.component';
import { UsuarioFormularioComponent } from './usuario-formulario/usuario-formulario.component';
import { PessoaService } from '../pessoa/pessoa.service';

@NgModule({
    declarations: [
        UsuarioListaComponent,
        UsuarioFormularioComponent
    ],
    imports: [
        // Angular
        HttpModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        UsuarioRouting
    ],
    providers: [
        // Serviços
        PessoaService,
        UsuarioService
    ]
})

export class UsuarioModule { }
