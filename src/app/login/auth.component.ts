import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from './auth.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private authService: AuthService,
    private messageService: MessageService,
  ) {
    if(authService.isAuthenticated())
      authService.logout();
  }

  ngOnInit() {
    /* Reactive Forms */
    this.loginForm = this.builder.group({
      username: this.builder.control('', [Validators.required]),
      password: this.builder.control('', [Validators.required]),

    }, {});


  }

  /**
   * Action form login
   * @param formData
   */
  login(formData) {
    this.authService.login(formData.username, formData.password)
  }

}
