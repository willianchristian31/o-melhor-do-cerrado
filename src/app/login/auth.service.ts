import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL: string = 'http://localhost:8888';

  constructor(
    private http: HttpClient,
    private router: Router,
    private messageService: MessageService,
  ) {
  }

  public login(username, password) {

    let loginData = {
      'username': username,
      'password': password
    };

    this.requestLogin(loginData).subscribe((data: any) => {

        let token = resp.headers.get('Authorization');

        localStorage.setItem('auth-token', token);

        this.router.navigate(['/']);

      },
      error => {
        let token = error.error.text;
        localStorage.setItem('auth-token', token);

        this.router.navigate(['/pessoa']);
        if(token != null) {
          if(token.length > 0 && token != 'undefined') {
            this.messageService.add({severity: 'success', summary: 'Bem vindo!'});
          } else {
            this.messageService.add({severity: 'warn', summary: 'Dados de acesso invalidados'});
          }
        } else {
          this.messageService.add({severity: 'warn', summary: 'Dados de acesso invalidados'});
        }
      });

  }

  public getAuthToken(): string {
    let token = localStorage.getItem('auth-token');
    return token;
  }

  public logout(): void {
    localStorage.removeItem('auth-token');
    this.messageService.add({severity: 'error', summary: 'Você saiu!'});
    this.router.navigate(['login']);
  }

  private requestLogin(loginData): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${this.URL}/login`, JSON.stringify(loginData), {observe: 'response'});
  }

  isAuthenticated(): Observable<boolean> | boolean {
    return (this.getAuthToken() && this.getAuthToken().length > 0 && this.getAuthToken() != 'undefined');
  }
}
