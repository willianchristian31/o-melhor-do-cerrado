import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthComponent} from './auth.component';



const produtoRoutes: Routes = [
  {path: '', component: AuthComponent},
];

@NgModule({
  imports: [RouterModule.forChild(produtoRoutes)],
  exports: [RouterModule]
})

export class AuthRouting {
}
