import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { CategoriaTuristica } from '../categoriaTuristica';
import { CategoriaTuristicaService } from '../categoriaTuristica.service';

@Component({
  selector: 'categoriaTuristica-lista',
  templateUrl: './categoriaTuristica-formulario.component.html',
  styleUrls: ['./categoriaTuristica-formulario.component.css']
})
export class CategoriaTuristicaFormularioComponent implements OnInit {

  categoriaTuristica: CategoriaTuristica;
  categoriaTuristicaForm: FormGroup;
  titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private categoriaTuristicaService: CategoriaTuristicaService
  ) { }

  ngOnInit() {

    this.categoriaTuristica = new CategoriaTuristica();

    /* Obter o `ID` passado por parâmetro na URL */
    this.categoriaTuristica.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.categoriaTuristica.id == null)
    ? 'Novo Ponto Turistico'
    : 'Alterar Ponto Turistico';

    /* Reactive Forms */
    this.categoriaTuristicaForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.categoriaTuristica.id != null) {
      this.categoriaTuristicaService.buscarPeloId(this.categoriaTuristica.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.categoriaTuristicaForm.patchValue(retorno);

        });
    }

  }

  salvar(categoriaTuristica: CategoriaTuristica) {
    if (this.categoriaTuristicaForm.invalid) {
      console.log("Erro no formulário");
    } 
    else {
      this.categoriaTuristicaService.salvar(categoriaTuristica)
      .subscribe(response => {
        console.log("Ponto Turistico salva com sucesso");

        // retorna para a lista
        this.router.navigate(['/categoriaTuristica']);
      },
      (error) => {
        console.log("Erro no back-end");
      });
    }
  }

}
