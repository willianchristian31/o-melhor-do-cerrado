import { Component, OnInit } from '@angular/core';

import { CategoriaTuristica } from '../categoriaTuristica';
import { CategoriaTuristicaService } from '../../categoriaTuristica/categoriaTuristica.service';
import { Router } from '@angular/router';

@Component({
  selector: 'categoriaTuristica-lista',
  templateUrl: './categoriaTuristica-lista.component.html',
  styleUrls: ['./categoriaTuristica-lista.component.css']
})
export class CategoriaTuristicaListaComponent implements OnInit {

    categoriaTuristicas: CategoriaTuristica[];

    constructor(
      private categoriaTuristicaService: CategoriaTuristicaService,
      private router: Router
    ){}

    ngOnInit() {
        
      this.categoriaTuristicaService.buscarTodos()
      .subscribe(resposta => {
        this.categoriaTuristicas = resposta
      });

    }

    excluir(categoriaTuristicaId: number) {
      this.categoriaTuristicaService.excluir(categoriaTuristicaId)
      .subscribe(resposta => {
        console.log("Ponto Turistico excluída com sucesso");
        // retorna para a lista
        this.router.navigate(['/categoriaTuristica']);
      } );
    }

}
