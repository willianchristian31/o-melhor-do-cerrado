import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { CategoriaTuristicaListaComponent } from './categoriaTuristica-lista/categoriaTuristica-lista.component';
import { CategoriaTuristicaFormularioComponent } from './categoriaTuristica-formulario/categoriaTuristica-formulario.component';

const categoriaTuristicaRoutes: Routes = [
    { path: '', component: CategoriaTuristicaListaComponent},
    { path: 'visualizar/:id', component: CategoriaTuristicaFormularioComponent},
    { path: 'novo', component: CategoriaTuristicaFormularioComponent},
    { path: 'alterar/:id', component: CategoriaTuristicaFormularioComponent},
];


@NgModule({
    imports: [RouterModule.forChild(categoriaTuristicaRoutes)],
    exports: [RouterModule]
  })

  export class CategoriaTuristicaRouting {}