import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { CategoriaTuristica } from './categoriaTuristica';

@Injectable()
export class CategoriaTuristicaService {

    private URL:string = "http://localhost:8888";

    constructor(private http: HttpClient) { }

    buscarTodos(): Observable<CategoriaTuristica[]> {
        return this.http
            .get<CategoriaTuristica[]>(`${this.URL}/categoriaTuristica`);
    }

    buscarPeloId(id: number): Observable<CategoriaTuristica> {
        return this.http
            .get<CategoriaTuristica>(`${this.URL}/categoriaTuristica/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(categoriaTuristica: CategoriaTuristica): Observable<CategoriaTuristica> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (categoriaTuristica.id) {
            return this.http
                .put<CategoriaTuristica>(
                    `${this.URL}/categoriaTuristica`,
                    JSON.stringify(categoriaTuristica),
                    httpOptions
                );
        } else {
            return this.http
                .post<CategoriaTuristica>(`${this.URL}/categoriaTuristica`, JSON.stringify(categoriaTuristica), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${this.URL}/categoriaTuristica/${id}`);
    }

}