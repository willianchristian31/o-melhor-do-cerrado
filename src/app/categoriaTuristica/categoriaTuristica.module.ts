import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { CategoriaTuristicaRouting } from './categoriaTuristica.routing';
import { CategoriaTuristicaService } from './categoriaTuristica.service';
import { CategoriaTuristicaListaComponent } from './categoriaTuristica-lista/categoriaTuristica-lista.component';
import { CategoriaTuristicaFormularioComponent } from './categoriaTuristica-formulario/categoriaTuristica-formulario.component';


@NgModule({
    declarations: [
        CategoriaTuristicaListaComponent,
        CategoriaTuristicaFormularioComponent
    ],
    imports: [
        // Angular
        HttpModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        CategoriaTuristicaRouting
    ],
    providers: [
        // Serviços
        CategoriaTuristicaService
    ]
})

export class CategoriaTuristicaModule { }