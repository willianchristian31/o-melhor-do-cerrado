import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthGuard } from './login/auth.guard';

const appRoutes: Routes = [
    { path: 'login', loadChildren: './login/auth.module#AuthModule' },
    { path: '', redirectTo: '/', pathMatch: 'full', canActivate: [AuthGuard]},
    { path: 'cidade', loadChildren: './cidade/cidade.module#CidadeModule', canActivate: [AuthGuard]},
    { path: 'pessoa', loadChildren: './pessoa/pessoa.module#PessoaModule', canActivate: [AuthGuard]},
    { path: 'usuario', loadChildren: './usuario/usuario.module#UsuarioModule', canActivate: [AuthGuard]},
    { path: 'pontoTuristico', loadChildren: './pontoTuristico/pontoTuristico.module#PontoTuristicoModule', canActivate: [AuthGuard]},
    { path: 'categoriaTuristica', loadChildren: './categoriaTuristica/categoriaTuristica.module#CategoriaTuristicaModule', canActivate: [AuthGuard]}
];



@NgModule({
    imports: [RouterModule.forRoot(
        appRoutes,
        { enableTracing: true }
    )],
    exports: [RouterModule]
  })

  export class AppRouting {}
