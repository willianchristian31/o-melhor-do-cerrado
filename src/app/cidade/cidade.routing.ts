import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { CidadeListaComponent } from './cidade-lista/cidade-lista.component';
import { CidadeFormularioComponent } from './cidade-formulario/cidade-formulario.component';

const cidadeRoutes: Routes = [
    { path: '', component: CidadeListaComponent},
    { path: 'visualizar/:id', component: CidadeFormularioComponent},
    { path: 'novo', component: CidadeFormularioComponent},
    { path: 'alterar/:id', component: CidadeFormularioComponent},
];


@NgModule({
    imports: [RouterModule.forChild(cidadeRoutes)],
    exports: [RouterModule]
  })

  export class CidadeRouting {}