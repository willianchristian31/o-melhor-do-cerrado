import { Component, OnInit } from '@angular/core';

import { Cidade } from '../cidade';
import { CidadeService } from '../../cidade/cidade.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cidade-lista',
  templateUrl: './cidade-lista.component.html',
  styleUrls: ['./cidade-lista.component.css']
})
export class CidadeListaComponent implements OnInit {

    cidades: Cidade[];

    constructor(
      private cidadeService: CidadeService,
      private router: Router
    ){}

    ngOnInit() {
        
      this.cidadeService.buscarTodos()
      .subscribe(resposta => {
        this.cidades = resposta
      });

    }

    excluir(cidadeId: number) {
      this.cidadeService.excluir(cidadeId)
      .subscribe(resposta => {
        console.log("Cidade excluída com sucesso");
        // retorna para a lista
        this.router.navigate(['/cidade']);
      } );
    }

}
