import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { CidadeRouting } from './cidade.routing';
import { CidadeService } from './cidade.service';
import { CidadeListaComponent } from './cidade-lista/cidade-lista.component';
import { CidadeFormularioComponent } from './cidade-formulario/cidade-formulario.component';


@NgModule({
    declarations: [
        CidadeListaComponent,
        CidadeFormularioComponent
    ],
    imports: [
        // Angular
        HttpModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        CidadeRouting
    ],
    providers: [
        // Serviços
        CidadeService
    ]
})

export class CidadeModule { }