import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { Cidade } from '../cidade';
import { CidadeService } from '../cidade.service';

@Component({
  selector: 'cidade-lista',
  templateUrl: './cidade-formulario.component.html',
  styleUrls: ['./cidade-formulario.component.css']
})
export class CidadeFormularioComponent implements OnInit {

  cidade: Cidade;
  cidadeForm: FormGroup;
  titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private cidadeService: CidadeService
  ) { }

  ngOnInit() {

    this.cidade = new Cidade();

    /* Obter o `ID` passado por parâmetro na URL */
    this.cidade.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.cidade.id == null)
    ? 'Nova Cidade'
    : 'Alterar Cidade';

    /* Reactive Forms */
    this.cidadeForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.cidade.id != null) {
      this.cidadeService.buscarPeloId(this.cidade.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.cidadeForm.patchValue(retorno);

        });
    }

  }

  salvar(cidade: Cidade) {
    if (this.cidadeForm.invalid) {
      console.log("Erro no formulário");
    } 
    else {
      this.cidadeService.salvar(cidade)
      .subscribe(response => {
        console.log("Curso salvo com sucesso");

        // retorna para a lista
        this.router.navigate(['/cidade']);
      },
      (error) => {
        console.log("Erro no back-end");
      });
    }
  }

}
