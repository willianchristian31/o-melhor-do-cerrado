import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { Cidade } from './cidade';

@Injectable()
export class CidadeService {

    private URL:string = "http://localhost:8888";

    constructor(private http: HttpClient) { }

    buscarTodos(): Observable<Cidade[]> {
        return this.http
            .get<Cidade[]>(`${this.URL}/cidade`);
    }

    buscarPeloId(id: number): Observable<Cidade> {
        return this.http
            .get<Cidade>(`${this.URL}/cidade/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(cidade: Cidade): Observable<Cidade> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (cidade.id) {
            return this.http
                .put<Cidade>(
                    `${this.URL}/cidade`, 
                    JSON.stringify(cidade), 
                    httpOptions
                );
        } else {
            return this.http
                .post<Cidade>(`${this.URL}/cidade`, JSON.stringify(cidade), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${this.URL}/cidade/${id}`);
    }

}