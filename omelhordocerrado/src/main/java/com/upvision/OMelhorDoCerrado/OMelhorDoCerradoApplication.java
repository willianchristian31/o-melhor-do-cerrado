package com.upvision.OMelhorDoCerrado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class OMelhorDoCerradoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OMelhorDoCerradoApplication.class, args);
	}
}
