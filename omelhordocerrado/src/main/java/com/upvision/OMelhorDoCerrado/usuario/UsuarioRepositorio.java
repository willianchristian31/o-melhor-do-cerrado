package com.upvision.OMelhorDoCerrado.usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import com.upvision.OMelhorDoCerrado.usuario.Usuario;

public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {

}
