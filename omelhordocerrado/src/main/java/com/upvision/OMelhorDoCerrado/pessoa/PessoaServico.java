package com.upvision.OMelhorDoCerrado.pessoa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PessoaServico {

    @Autowired
    private PessoaRepositorio pessoaRepositorio;


    public Pessoa salvar(Pessoa pessoa){
        return pessoaRepositorio.save(pessoa);
    }

    public boolean excluir(Long pessoaId){
        Pessoa pessoaBusca = pessoaRepositorio.getOne(pessoaId);
        if( pessoaBusca != null ){
            pessoaRepositorio.delete(pessoaBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Pessoa buscarPeloId(Long pessoaId){
        return pessoaRepositorio.getOne(pessoaId);
    }

    public List<Pessoa> buscarTodos(){
        return pessoaRepositorio.findAll();
    }

}
