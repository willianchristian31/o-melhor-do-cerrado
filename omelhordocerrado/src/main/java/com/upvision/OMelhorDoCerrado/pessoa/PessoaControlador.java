package com.upvision.OMelhorDoCerrado.pessoa;
import com.upvision.OMelhorDoCerrado.usuario.Usuario;
import com.upvision.OMelhorDoCerrado.pessoa.Pessoa;
import com.upvision.OMelhorDoCerrado.pessoa.PessoaServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pessoa")
public class PessoaControlador {

    @Autowired
    private PessoaServico pessoaServico;

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody Pessoa pessoa){
        return new ResponseEntity(pessoaServico.salvar(pessoa), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody Pessoa pessoa){
        return new ResponseEntity(pessoaServico.salvar(pessoa), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{pessoaId}")
    public ResponseEntity<?> excluir(@PathVariable Long pessoaId) {
        return new ResponseEntity(pessoaServico.excluir(pessoaId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return new ResponseEntity(pessoaServico.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(value = "/{pessoaId}")
    public ResponseEntity<?> findById(@PathVariable Long pessoaId) {
        return new ResponseEntity(pessoaServico.buscarPeloId(pessoaId), HttpStatus.OK);
    }

}
