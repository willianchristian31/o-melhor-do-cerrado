package com.upvision.OMelhorDoCerrado.pontoTuristico;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.internal.NotNull;
import com.upvision.OMelhorDoCerrado.categoriaTuristica.CategoriaTuristica;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pontoTuristico")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PontoTuristico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pontoTuristico_id_seq")
    @SequenceGenerator(name = "pontoTuristico_id_seq", sequenceName = "pontoTuristico_id_seq", allocationSize = 1)
    @Column(name = "id")
    private long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "descricao")
    private String descricao;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoriaTuristica_id", referencedColumnName = "id")
    private CategoriaTuristica categoriaTuristica;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}