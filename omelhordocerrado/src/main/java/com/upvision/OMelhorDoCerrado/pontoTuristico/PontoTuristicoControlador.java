package com.upvision.OMelhorDoCerrado.pontoTuristico;

import com.upvision.OMelhorDoCerrado.pontoTuristico.PontoTuristico;
import com.upvision.OMelhorDoCerrado.pontoTuristico.PontoTuristicoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pontoTuristico")
public class PontoTuristicoControlador {

    @Autowired
    private PontoTuristicoServico pontoTuristicoServico;

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody PontoTuristico pontoTuristico){
        return new ResponseEntity(pontoTuristicoServico.salvar(pontoTuristico), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody PontoTuristico pontoTuristico){
        return new ResponseEntity(pontoTuristicoServico.salvar(pontoTuristico), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{pontoTuristicoId}")
    public ResponseEntity<?> excluir(@PathVariable Long pontoTuristicoId) {
        return new ResponseEntity(pontoTuristicoServico.excluir(pontoTuristicoId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return new ResponseEntity(pontoTuristicoServico.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(value = "/{pontoTuristicoId}")
    public ResponseEntity<?> findById(@PathVariable Long pontoTuristicoId) {
        return new ResponseEntity(pontoTuristicoServico.buscarPeloId(pontoTuristicoId), HttpStatus.OK);
    }

}
