package com.upvision.OMelhorDoCerrado.pontoTuristico;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PontoTuristicoRepositorio extends JpaRepository<PontoTuristico, Long> {
}
