package com.upvision.OMelhorDoCerrado.pontoTuristico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PontoTuristicoServico {

    @Autowired
    private PontoTuristicoRepositorio pontoTuristicoRepositorio;


    public PontoTuristico salvar(PontoTuristico pontoTuristico){
        return pontoTuristicoRepositorio.save(pontoTuristico);
    }

    public boolean excluir(Long pontoTuristicoId){
        PontoTuristico pontoTuristicoBusca = pontoTuristicoRepositorio.getOne(pontoTuristicoId);
        if( pontoTuristicoBusca != null ){
            pontoTuristicoRepositorio.delete(pontoTuristicoBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public PontoTuristico buscarPeloId(Long pontoTuristicoId){
        return pontoTuristicoRepositorio.getOne(pontoTuristicoId);
    }

    public List<PontoTuristico> buscarTodos(){
        return pontoTuristicoRepositorio.findAll();
    }

}
