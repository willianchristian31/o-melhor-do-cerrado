package com.upvision.OMelhorDoCerrado.cidade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadeServico {

    @Autowired
    private CidadeRepositorio cidadeRepositorio;


    public Cidade salvar(Cidade cidade){
        return cidadeRepositorio.save(cidade);
    }

    public boolean excluir(Long cidadeId){
        Cidade cidadeBusca = cidadeRepositorio.getOne(cidadeId);
        if( cidadeBusca != null ){
            cidadeRepositorio.delete(cidadeBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public Cidade buscarPeloId(Long cidadeId){
        return cidadeRepositorio.getOne(cidadeId);
    }

    public List<Cidade> buscarTodos(){
        return cidadeRepositorio.findAll();
    }

}
