package com.upvision.OMelhorDoCerrado.cidade;

import com.upvision.OMelhorDoCerrado.cidade.Cidade;
import com.upvision.OMelhorDoCerrado.cidade.CidadeServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cidade")
public class CidadeControlador {

    @Autowired
    private CidadeServico cidadeServico;

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody Cidade cidade){
        return new ResponseEntity(cidadeServico.salvar(cidade), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody Cidade cidade){
        return new ResponseEntity(cidadeServico.salvar(cidade), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{cidadeId}")
    public ResponseEntity<?> excluir(@PathVariable Long cidadeId) {
        return new ResponseEntity(cidadeServico.excluir(cidadeId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return new ResponseEntity(cidadeServico.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(value = "/{cidadeId}")
    public ResponseEntity<?> findById(@PathVariable Long cidadeId) {
        return new ResponseEntity(cidadeServico.buscarPeloId(cidadeId), HttpStatus.OK);
    }

}
