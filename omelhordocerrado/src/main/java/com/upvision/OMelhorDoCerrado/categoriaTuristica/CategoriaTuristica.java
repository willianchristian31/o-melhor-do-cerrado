package com.upvision.OMelhorDoCerrado.categoriaTuristica;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.internal.NotNull;
import com.upvision.OMelhorDoCerrado.pontoTuristico.PontoTuristico;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "categoriaTuristica")
@JsonIgnoreProperties({"pontosTuristicos", "hibernateLazyInitializer", "handler"})
public class CategoriaTuristica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categoriaTuristica_id_seq")
    @SequenceGenerator(name = "categoriaTuristica_id_seq", sequenceName = "categoriaTuristica_id_seq", allocationSize = 1)
    @Column(name = "id")
    private long id;

    @Column(name = "nome")
    private String nome;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "categoriaTuristica")
    private List<PontoTuristico> pontoTuristico;


    public List<PontoTuristico> getPontoTuristico() {
        return pontoTuristico;
    }

    public void setPontoTuristico(List<PontoTuristico> pontoTuristico) {
        this.pontoTuristico = pontoTuristico;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}