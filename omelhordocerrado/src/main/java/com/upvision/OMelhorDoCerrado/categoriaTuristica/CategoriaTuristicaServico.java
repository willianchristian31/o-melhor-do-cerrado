package com.upvision.OMelhorDoCerrado.categoriaTuristica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaTuristicaServico {

    @Autowired
    private CategoriaTuristicaRepositorio categoriaTuristicaRepositorio;


    public CategoriaTuristica salvar(CategoriaTuristica categoriaTuristica){
        return categoriaTuristicaRepositorio.save(categoriaTuristica);
    }

    public boolean excluir(Long categoriaTuristicaId){
        CategoriaTuristica categoriaTuristicaBusca = categoriaTuristicaRepositorio.getOne(categoriaTuristicaId);
        if( categoriaTuristicaBusca != null ){
            categoriaTuristicaRepositorio.delete(categoriaTuristicaBusca);
            return true;
        }
        else {
            return false;
        }
    }

    public CategoriaTuristica buscarPeloId(Long categoriaTuristicaId){
        return categoriaTuristicaRepositorio.getOne(categoriaTuristicaId);
    }

    public List<CategoriaTuristica> buscarTodos(){
        return categoriaTuristicaRepositorio.findAll();
    }

}
