package com.upvision.OMelhorDoCerrado.categoriaTuristica;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriaTuristicaRepositorio extends JpaRepository<CategoriaTuristica, Long> {
}
