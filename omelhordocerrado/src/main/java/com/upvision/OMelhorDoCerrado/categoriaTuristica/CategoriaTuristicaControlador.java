package com.upvision.OMelhorDoCerrado.categoriaTuristica;

import com.upvision.OMelhorDoCerrado.pontoTuristico.PontoTuristico;
import com.upvision.OMelhorDoCerrado.pontoTuristico.PontoTuristicoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categoriaTuristica")
public class CategoriaTuristicaControlador {

    @Autowired
    private CategoriaTuristicaServico categoriaTuristicaServico;

    @PostMapping
    public ResponseEntity<?> salvar(@Validated @RequestBody CategoriaTuristica categoriaTuristica){
        return new ResponseEntity(categoriaTuristicaServico.salvar(categoriaTuristica), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> alterar(@Validated @RequestBody CategoriaTuristica categoriaTuristica){
        return new ResponseEntity(categoriaTuristicaServico.salvar(categoriaTuristica), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{categoriaTuristicaId}")
    public ResponseEntity<?> excluir(@PathVariable Long categoriaTuristicaId) {
        return new ResponseEntity(categoriaTuristicaServico.excluir(categoriaTuristicaId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return new ResponseEntity(categoriaTuristicaServico.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(value = "/{categoriaTuristicaId}")
    public ResponseEntity<?> findById(@PathVariable Long categoriaTuristicaId) {
        return new ResponseEntity(categoriaTuristicaServico.buscarPeloId(categoriaTuristicaId), HttpStatus.OK);
    }

}
